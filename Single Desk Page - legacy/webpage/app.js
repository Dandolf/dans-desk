var onCurrentDay = true;
var graphRefresh = 5;

$(document).ready(function() {
    $('#datePicker').datepicker({
        dateFormat: 'mm-dd-yy'
    });
    $('#datePicker').change(function(){
        var queryDate = $('#datePicker').val();
        if (queryDate == new Date().today()) {
            onCurrentDay = true;
            setTimeout(updateGraph, graphRefresh * 6000);
        }
        else {
            onCurrentDay = false;
        }
        $.get('/getVoltageHistoryByDate?date=' + queryDate, function(data){
            var parsedData = JSON.parse(data);
            if (parsedData.length > 0){
                graphData(parsedData);
            }
            else {
                alert('No Data for this Date');
            }
        });
    });

    $('#dateButton').click(function(){
        $('#datePicker').show().focus().hide();
        $('#datePicker').datepicker('show');
    });

    $.get('/getVoltageHistory', function(data){
        var parsedData = JSON.parse(data);
        graphData(parsedData);
        setTimeout(updateGraph, graphRefresh * 60000);
    });
});

function graphData(data) {
    var graphDiv = $('#graph')[0];

    var formatedData = formatData(data);

    var plotlyJSON = {
        data : [{
            visible: true,
            type: 'scatter',
            name: 'B',
            hoverinfo: 'x+y+z+text',
            x:formatedData.x,
            y:formatedData.y,
            xcalendar: 'gregorian',
            ycalendar: 'gregorian',
            text: '',
            // mode: 'lines+markers',
            mode: 'lines',
            line: {
                color: '#1f77b4',
                width: 4,
                dash: 'solid',
                shape: 'linear',
                simplify: true
            },
            connectgaps: false,
            marker: {
                symbol: 'circle',
                opacity: 1,
                size: 10,
                color: '#1f77b4',
                line: {
                    color: '#444',
                    width: 0
                },
                maxdisplayed: 0
            },
            fill: 'tonexty',
            fillcolor: 'rgba(31, 119, 180, 0.5)'

        }],
        layout: {
            title: formatedData.date,
            autosize: true,
            calendar: 'gregorian',
            xaxis: {
                title: 'Time'
            },
            yaxis: {
                title: 'Voltage'
            }
        }
    };

    Plotly.newPlot(
        graphDiv,
        plotlyJSON
    );
}

function formatData(data) {
    // data comes in as [{timeStamp:'', voltage:''}...]
    // data comes out as [{x:[...], y:[...]}, date:'...']
    var formatedData = {x:[], y:[]};
    for (var i = 0; i < data.length; i++) {
        var time = data[i].timeStamp.split(' @ ')[1];
        formatedData.x.push(time);
        formatedData.y.push(data[i].voltage);
    }
    formatedData.date = data[0].timeStamp.split(' @ ')[0];
    return formatedData;
}

function updateGraph(){
    if (onCurrentDay){
        $.get('/getVoltageHistory', function(data){
            var parsedData = JSON.parse(data);
            graphData(parsedData);
            setTimeout(updateGraph, graphRefresh * 60000);
        });
    }
}

// For todays date;
Date.prototype.today = function () {
    return (((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"-"+ ((this.getDate() < 10)?"0":"") + this.getDate() +"-"+ this.getFullYear();
}