// ##########################
// Dependancies
// ##########################
var express = require('express');
var SerialPort = require("serialport");
var fs = require('fs');
var httpProxy = require('http-proxy');

// ##########################
// Constants and Variables
// ##########################
// configuration constants
var pollingFrequency = 5; //in minutes
var dataFolder = __dirname + '/data/';

// app variables
var dataArray;

// ##########################
// Server Setup
// ##########################
var app = express();
var serverPort = 80;
app.use(express.static(__dirname+'/webpage'));

// ##########################
// Proxy Setup
// ##########################
var pingPongProxyAddress = 'http://localhost:3000';
var proxy = httpProxy.createProxyServer({});

// ##########################
// Serial Port Setup
// ##########################
SerialPort.list(function (err, ports) {
  ports.forEach(function(port) {
    //console.log(port.comName);
  });
});

var port = new SerialPort("/dev/ttyUSB0", {
  baudRate: 9600,
  parser: SerialPort.parsers.readline('\n')
});

// ##########################
// Serial Port Events
// ##########################
// when serial port first opens
port.on('open', function() {
  console.log ('Serial Port Opened');
  console.log ('Starting Polling...');
  //port.write('V');  //arduino is not yet ready since it resets on new serial connection

  // load up any existing data for the current day
  var currentFile = dataFolder + new Date().today() + '.txt';
  dataArray = loadDataFile(currentFile);

  // start polling for data
  setInterval(function(){
    port.write('V');
  }, 60000 * pollingFrequency);
});

// when new data is incomming from serial port
port.on('data', function(data) {
  if (data[0] = 'V') {
    updateLog(parseFloat(data.substr(1)));
    console.log(data.substring(1));
  }
});

// when serial port encounters an error
port.on('error', function(err) {
  if (err.message.indexOf('cannot open /dev/ttyUSB0') > -1) {
    console.log('Could not connect to USB Voltmeter');
  }
  console.log('Error: ', err.message);
});

// ##########################
// Express Endpoints
// ##########################
// returns most recent voltage reading
app.get('/getVoltage', function (req, res) {
  var response = 'Loading';
  if (dataArray.length) {
    response = JSON.stringify(dataArray[dataArray.length -1].voltage);
  }
  res.end(response);
});

// returns entire days worth of data
app.get('/getVoltageHistory', function(req, res) {
  var response = JSON.stringify(dataArray);
  res.end(response);
});

app.get('/getVoltageHistoryByDate', function(req, res) {
  var date = req.query.date;
  var response =  loadDataFile(dataFolder + date + '.txt');
  res.end(JSON.stringify(response));
});

app.get('/sensorData', function(req, res) {
  proxy.web(req, res, {target: pingPongProxyAddress});
});

// returns webpage frontend
app.get('/', function(req, res){
        res.sendFile(__dirname+'/index.html');
});

// start server
app.listen(serverPort, function () {
  console.log('App listening on port '+serverPort);
});

// ##########################
// Helper Functions
// ##########################
// creates a new entry for new data and writes it to file
// if it is a new day (there is no file for current date), clear app data cache
function updateLog(voltage) {
  var currentFile = dataFolder + new Date().today() + '.txt';
  if (!fs.existsSync(currentFile)) {
    dataArray = [];
  }
  var dataToPush = {
    timeStamp : new Date().today() + " @ " + new Date().timeNow(),
    voltage: voltage
  };
  dataArray.push(dataToPush);
  writeDataFile(currentFile, dataArray);
}

// write data to given file
// if file doesn't exist, create it
function writeDataFile(filePath, data) {
    fs.writeFile(filePath, JSON.stringify(data), function(err) {
        if (err) {
            console.log('write error: ' +err.message);
        }
    });
}

// load data from file if file exists
function loadDataFile(filePath) {
    var readData = [];
    if (fs.existsSync(filePath)) {
      var readData = JSON.parse(fs.readFileSync(filePath).toString());
    }
    return readData;
}

// For todays date;
Date.prototype.today = function () {
    return (((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"-"+ ((this.getDate() < 10)?"0":"") + this.getDate() +"-"+ this.getFullYear();
    //return ((this.getDate() < 10)?"0":"") + this.getDate() +"-"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"-"+ this.getFullYear();
}

// For the time now
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}