var zoneData = [
    {
        name: "Happy Zone",
        desks: [
            {
                name: "Dan's Desk",
                module: "M13354588",
                tableNumber: "table1",
                currentVoltage: 12.05
            },
            {
                name: "Tyler's Desk",
                module: "M13354588",
                tableNumber: "table2",
                currentVoltage: 10.05
            }
        ]
    },
    {
        name: "Sad Zone",
        desks: [
            {
                name: "Sean's Desk",
                module: "M13354588",
                tableNumber: "table3",
                currentVoltage: 5.05
            },
            {
                name: "Greg's Desk",
                module: "M13354588",
                tableNumber: "table4",
                currentVoltage: 6.05
            }
        ]
    }
];

$(document).ready(function() {
    for (var i = 0; i < zoneData.length; i++) {
        buildZone(zoneData[i], i);
        $('#zone'+i).click(function(){
            var target = $(this);
            if (target.hasClass('zoneCompressed')){
                target.find('.deskCompressedContainer').hide();
                target.find('.deskExpandedContainer').show();
            }
        });

        // if clicking outside zone, collapse all zones
        $(document).mouseup(function(e){
            
            if (!$('zoneCompressed').is(e.target)) {
                var target = $(this);
                target.find('.deskCompressedContainer').hide();
                target.find('.deskExpandedContainer').show();
            }
        })
    }
});

function buildZone(zone, number) {
   var container = $('<div id="zone'+number+'" class="zoneCompressed col-md-7"></div>');
   container.append($('<h3 class="zoneName">'+zone.name+'</h3>'));
   var deskCompressedDiv = $('<div class="deskCompressedContainer"></div>');
   var deskExpandedDiv = $('<div class="deskExpandedContainer container"></div>');
   for (var i = 0; i < zone.desks.length; i++) {
       deskCompressedDiv.append($('<div class="deskCompressed"></div>'));
       var row = $('<div class="deskExpanded row"></div>');
       row.append($('<div class="col-md-6"></div>').append('<h4>'+zone.desks[i].name+'<h4>'));
       row.append($('<div class="col-md-6"></div>').append('<h4><b>'+zone.desks[i].currentVoltage+'V</b><h4>'));
       deskExpandedDiv.append(row);
   }
   deskExpandedDiv.hide();
   container.append(deskCompressedDiv);
   container.append(deskExpandedDiv);
   $('#zoneContainer').append(container);
}
