// ##########################
// Dependancies
// ##########################
var express = require('express');
var http = require('http');
var rp = require('request-promise');
var bluebird = require('bluebird');
var os = require('os');

// ##########################
// Server Setup
// ##########################
var app = express();
var networkInterfaces = os.networkInterfaces();
var address = networkInterfaces['Wi-Fi'][1].address;
var port = 80;
// app.use(express.static(__dirname+'/webpage'));
app.use(express.static(__dirname+'/angularPage'));

// ##########################
// Constants and Variables
// ##########################
// configuration constants
var moduleCheckInTimeout = 10000;
var dataFolder = __dirname + '/data/';

// app variables
var nodeIPs = {};
var nodeData = {};

// ##########################
// Express Routes/Endpoints
// ##########################
// Endpoints for communicating with hardware nodes
var nodeRouter = express.Router();
nodeRouter.route('/checkIn/:chipID/:chipIP')
    .get(function(req, res){
        console.log(req.path);
        //console.log('chipID: ' + req.params.chipID + ' chipIP: ' + req.params.chipIP);
        nodeIPs[req.params.chipID] = req.params.chipIP;
        res.end('R,'+moduleCheckInTimeout);
    });

// Endpoints for client/browser communication
var apiRouter = express.Router();
apiRouter.route('/getNodes')
    .get(function(req, res){
        console.log(req.path);
        res.end(JSON.stringify(nodeIPs));
    });

apiRouter.route('/queryData')
    .get(function(req, res){
        console.log(req.path);
        queryAllNodes(function(data){            
            res.end(JSON.stringify(data));
        });
    });

// returns webpage frontend
app.get('/', function(req, res){
        res.sendFile(__dirname+'/index.html');
});

// Register routes
app.use('/node', nodeRouter);
app.use('/api', apiRouter);

// Start server
app.listen(port, function(){    
    console.log('Server is running at '+address+':'+port);
});

// ##########################
// Helper Functions
// ##########################
// Sends requests to all hardware nodes for voltage data
// Uses Promises to verify all information is received
function queryAllNodes(callback) {
    var requests = [];
    for (var key in nodeIPs) {
        if (nodeIPs.hasOwnProperty(key)) {
            requests.push(
                rp({
                    method: 'GET',
                    uri: 'http://' + nodeIPs[key] + '/getVoltage'
                })
                .then(function (data) {                    
                    nodeData[key] = cleanDataString(data);
                    console.log(nodeData);
                })
            );
        }
    }
    bluebird.all(requests)
    .then(function() {
        callback(nodeData);
    });
}

// Converts data string from hardware node into JSON
function cleanDataString (data) {
    var cleanedString = data.replace('V', '');
    cleanedString = cleanedString.replace('\r\n', '');
    var dataArray = cleanedString.split(',');
    var dataObject = {};
    for (var i = 0; i < dataArray.length; i++) {
        dataObject['table' + (i + 1)] = dataArray[i];
    }
    return dataObject;
}
